package org.irmacard.api.web.sessions;

import org.irmacard.api.common.disclosure.DisclosureProofRequest;
import org.irmacard.api.common.disclosure.ServiceProviderRequest;

public class BroadcastSession extends IrmaSession<ServiceProviderRequest, DisclosureProofRequest> {

    public BroadcastSession() {
        super();
    }
}
