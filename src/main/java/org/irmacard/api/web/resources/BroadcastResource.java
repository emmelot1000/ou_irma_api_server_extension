package org.irmacard.api.web.resources;

import org.irmacard.api.common.ClientQr;
import org.irmacard.api.common.JwtSessionRequest;
import org.irmacard.api.common.ProtocolVersion;
import org.irmacard.api.common.disclosure.DisclosureProofRequest;
import org.irmacard.api.common.disclosure.ServiceProviderRequest;
import org.irmacard.api.common.exceptions.ApiError;
import org.irmacard.api.common.exceptions.ApiException;
import org.irmacard.api.web.sessions.BroadcastSession;
import org.irmacard.api.web.sessions.IrmaSession;
import org.irmacard.api.web.sessions.Sessions;
import org.irmacard.api.web.sessions.VerificationSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("broadcast")
public class BroadcastResource extends BaseResource
        <DisclosureProofRequest, ServiceProviderRequest, BroadcastSession> {
    private static Logger logger = LoggerFactory.getLogger(BroadcastResource.class);
    private static final int DEFAULT_TOKEN_VALIDITY = 60 * 60; // 1 hour
    private String state;
    private ProtocolVersion version;

    @Inject
    public BroadcastResource() {
        //Create a BaseResource for this broadcasting action
        super(Action.BROADCASTING, Sessions.getBroadcastSessions());
        logger.debug("Started broadcast resource");
    }

    @POST
    @Consumes({MediaType.TEXT_PLAIN,MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public ClientQr newSession(String jwt) {
        logger.debug("Creating new session with jwt: " + jwt);
        //Set the state to BROADCAST, so a broadcast session will be created
        state = "BROADCAST";
        //Return the broadcast session
        return super.newSession(jwt);
    }

    @GET
    @Path("/{sessiontoken}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public DisclosureProofRequest get(@PathParam("sessiontoken") String sessiontoken,
                                      @HeaderParam("X-IRMA-MinProtocolVersion") ProtocolVersion minVersion,
                                      @HeaderParam("X-IRMA-MaxProtocolVersion") ProtocolVersion maxVersion) {
        return super.get(sessiontoken, minVersion, maxVersion);
    }

    @GET @Path("/{sessiontoken}/jwt")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public JwtSessionRequest getJwt(@PathParam("sessiontoken") String sessiontoken, @HeaderParam("X-IRMA-ProtocolVersion") ProtocolVersion version) {
        return super.getJwt(sessiontoken, version);
    }

    @GET @Path("/{sessiontoken}/status")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public IrmaSession.Status getStatus(@PathParam("sessiontoken") String sessiontoken) {
        return super.getStatus(sessiontoken);
    }

    @GET @Path("/{sessiontoken}/unique")
    @Produces(MediaType.APPLICATION_JSON)
    public ClientQr getUniqueSessionToken(@PathParam("sessiontoken") String sessiontoken) {
        Sessions<BroadcastSession> broadcastSessions  = Sessions.getBroadcastSessions();
        //Show what broadcast sessions are available
        broadcastSessions.print();
        //Find the broadcast session in the available sessions
        IrmaSession session = Sessions.findAnySession(sessiontoken);
        //Retrieve the jwt of the broadcast session so it can be used for the unique sessions
        String broadcastJwt = session.getJwt();
        version = session.getVersion();
        logger.debug("Retrieved the broadcast token: " + broadcastJwt);
        //Set state to UNIQUE so a unique session will be created
        state = "UNIQUE";
        //Create a new session using this jwt and return the QR
        return super.newSession(broadcastJwt);

    }

    @DELETE @Path("/{sessiontoken}")
    @Override
    public void delete(@PathParam("sessiontoken") String sessiontoken) {
        super.delete(sessiontoken);
    }

    @Override
    protected ClientQr create(ServiceProviderRequest spRequest, String verifier, String jwt) {
        DisclosureProofRequest request = spRequest.getRequest();
        //Request must contain data
        if (request == null || request.getContent() == null || request.getContent().size() == 0)
            throw new ApiException(ApiError.MALFORMED_VERIFIER_REQUEST);
        // Check if the requested attributes match the DescriptionStore
        if (!request.attributesMatchStore())
            throw new ApiException(ApiError.ATTRIBUTES_WRONG);
        //Check if the validity has been set, else give default validity
        if (spRequest.getValidity() == 0)
            spRequest.setValidity(DEFAULT_TOKEN_VALIDITY);

        ClientQr qr = null;
        //Create the qr depending on the current state
        if (state == "BROADCAST") {
            BroadcastSession session = new BroadcastSession();
            qr = super.create(session, spRequest, jwt);
        }
        if (state == "UNIQUE") {
            VerificationSession verificationSession = new VerificationSession();
            qr = super.createUniqueSession(verificationSession, spRequest, jwt, version);
        }
        return qr;
    }
}
