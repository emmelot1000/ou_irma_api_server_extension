Please note that this a largely a copy of the IRMA API server code written by Privacy By Design. We have only adapted the code to make it work for broadcasting sessions.

Our additions:
* /src/main/java/org/irmacard/api/web/resources/BroadcastResource.java
* /src/main/java/org/irmacard/api/web/sessions/BroadcastSession.java


Our changes:
* /src/main/java/org/irmacard/api/web/ApiConfiguration.java
* /src/main/java/org/irmacard/api/web/resources/BaseResource.java
* /src/main/java/org/irmacard/api/web/sessions/Sessions.java


